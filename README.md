# React Tech Challenge

## Introduction

This is a fork of the [coding challenge from Servall Development](https://gitlab.com/servall/tech-challenge/react-challenge).

It is an implementation of an MP Lookup App (lookup is done by entering postal code of MP's riding).


## Setup

1. Clone to your local machine by running [`git clone`](https://git-scm.com/docs/git-clone) or your git tool of choice.

2. Install the app by running in a command prompt or terminal (Note: [NodeJS](https://nodejs.org/en/) needs to be installed):

      `npm install`   
      or   
      `yarn install`   

3. Start the development environment by then running:

      `npm start`   
      or   
      `yarn start`   


## Technologies Used

- Uses [React framework](https://reactjs.org/docs/hello-world.html), [Redux](https://redux.js.org/), [Redux Thunk](https://github.com/reduxjs/redux-thunk), [fetch-jsonp](https://www.npmjs.com/package/fetch-jsonp)
- Based on the [Create React App](https://github.com/facebookincubator/create-react-app)
- Representative API from [opennorth.ca](https://represent.opennorth.ca/api/)
- Data returned in the [JSONP format](https://stackoverflow.com/questions/3839966/can-anyone-explain-what-jsonp-is-in-layman-terms)

## TODO

These are some additions that I would like to apply to improve this app in the future:

- Create JEST integration tests using Enzyme
- Use React Router to allow for the ability to use the browser's back/forward buttons to navigate between previous searches
- Consider a more attractive user interface


## Additional Notes

Using Redux to develop an app of this size is probably not worth the overhead and added complexity.  Nevertheless, I wanted to use Redux because I like Redux's pattern of controlling app state, and to be honest, I wanted to practice and demonstrate my use of Redux in an app that performs asynchronous calls to an API.  Additionally, I find Redux useful for app scalability.
