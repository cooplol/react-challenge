import fetchJsonp from 'fetch-jsonp';

import { FETCH_DATA, SET_ERROR } from 'actions/types';

export function fetchMPData(postalCode) {
	postalCode = postalCode.toUpperCase();

	const url = `https://represent.opennorth.ca/postcodes/${postalCode}?sets=federal-electoral-districts`;

	return dispatch => {
		fetchJsonp(url)
			.then(resp => resp.json())
			.then(json => {
				dispatch({
					type: FETCH_DATA,
					payload: json
				});
			})
			.catch(er => {
				dispatch({
					type: SET_ERROR,
					payload: '404'
				});
			});
	};
}

export function setError(errorCode) {
	return {
		type: SET_ERROR,
		payload: errorCode	//invalid postal code
	};
}




//
// function updateMPDataState(code) {
// 		return {
// 			type: UPDATE_DATA_STATE,
// 			payload: code
// 		};
// }
