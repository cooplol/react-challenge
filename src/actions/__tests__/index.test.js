import { setError } from 'actions';
import { SET_ERROR } from 'actions/types';

describe('setError', () => {
  it('has the correct type', () => {
    const action = setError();

    expect(action.type).toEqual('SET_ERROR');
  });

  it('has the correct payload', () => {
    const action = setError('404');

    expect(action.payload).toEqual('404');
  });
});
