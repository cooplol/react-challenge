export const FETCH_DATA = 'fetch-data';
export const SET_ERROR = 'set-error';

export const UPDATE_DATA_STATE = 'update-data-state';
