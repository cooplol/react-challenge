import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as actions from 'actions';


class SearchBox extends Component {
	state = {
		input: ''
	};

	handleSubmit = event => {
		event.preventDefault();

		//make user input restrcitions more forgiving
		//(allow for whitespce, non-capital characters);
		//convert to proper format before submit
		let formattedPostalCode = this.state.input
			.replace(/\s/g, '')
			.toUpperCase();

		//do some minor form validation
		//(I think use of redux-form would only be
		//useful if this form had more than one input element)
		if (formattedPostalCode.length !== 6) {
			this.setState({ errMsg: 'Postal code must be 6 characters.' });
			return false;
		}
		else if (!formattedPostalCode.match(/^[a-z0-9]+$/i)) {
			this.setState({ errMsg: 'Postal code must alphanumeric.' })
				return false;
		}

		//don't use history.push because we want mpdata
		//component to mount (not just update) to make it
		//consistent when user just goes to url with postal
		//code or is sent to it from searchbar
		window.location.href = '/' + formattedPostalCode;
	}

	handleChange = event => {
		this.setState({
			input: event.target.value,
			errMsg: ''
		 });
	}

	render() {
		return (
			<nav className="navbar justify-content-center">
  			<form onSubmit={this.handleSubmit}>
					<div className="input-group">
						<input
							className="form-control searchbar"
							type="search"
							placeholder="Enter Postal Code"
							value={this.state.input}
							onChange={this.handleChange}
						/>
						<span className="input-group-btn">
    					<button className="btn btn-default btn-submit"  type="submit">Search</button>
						</span>
					</div>
					<div className="err-msg text-danger">
						{this.state.errMsg}
					</div>
  			</form>
			</nav>
		);
	}
}

export default connect(null, actions)(withRouter(SearchBox));
