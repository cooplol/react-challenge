import React from 'react';

import SearchBox from 'components/SearchBox';

export default ({ children }) => {
	return(
		<div>
			<SearchBox />
			{children}
		</div>
	);
}
