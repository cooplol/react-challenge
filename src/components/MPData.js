import React, { Component } from 'react';
import { connect } from 'react-redux';

import { INVALID_POSTAL_CODE, POSTAL_CODE_NOT_FOUND } from 'errorCodes';
import * as actions from 'actions';


class MPData extends Component {
	componentDidMount() {
		const { postalCode } = this.props.match.params;

		//set error if postal code is invalid
		if (postalCode && (
			postalCode.length !== 6 ||
			!postalCode.match(/^[a-z0-9]+$/i))
		) {
				//call invalid postal code error action creator
				this.props.setError(INVALID_POSTAL_CODE);
		} else {
			this.props.fetchMPData(postalCode);
		}
	}


	render() {
		const { mpdata, error } = this.props.data;
		const { postalCode } = this.props.match.params;

		if (error) {
			switch(error) {
				case INVALID_POSTAL_CODE:
				case POSTAL_CODE_NOT_FOUND:
					return(
						<div>'{postalCode}' is invalid.</div>
					);
				default:
					return(
						<div>an unknonw error occured</div>
					);
			}
		}


		if (mpdata) {
			const { city, province } = mpdata;
			const { name, email, party_name, photo_url } =
				mpdata.representatives_centroid[0];

			return(
				<div>
					<h3>MP Info</h3>
					<ul>
						<li>
							<h4>Name</h4>
							<div>{name}</div>
							<img src={photo_url} alt="{name}" />
						</li>
						<li>
							<h4>Email</h4>
							<div><a href={`mailto:${email}`}>{email}</a></div>
						</li>
						<li>
							<h4>Political Party</h4>
							<div>{party_name}</div>
						</li>
						<li>
							<h4>Location</h4>
							<div>{`${city}, ${province}`}</div>
						</li>
					</ul>
				</div>
			);
		} else {		//mpdata is not available
			return <div>loading...</div>
		}
	}
}

function mapStateToProps({ data }) {
	return { data };
}

export default connect(mapStateToProps, actions)(MPData);
