import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter, BrowserRouter, Route, Switch } from 'react-router-dom';

import App from 'components/App';
import Root from 'Root';
import Welcome from 'components/Welcome';
import MPData from 'components/MPData';
import SearchBox from 'components/SearchBox';


it('shows welcome screen at index', () => {
	 const wrapped = mount(
	    <Root>
				<MemoryRouter initialEntries={[ '/' ]} >
					<App>
						<Route path="/" exact component={Welcome} />
						<Route path="/:postalCode" exact component={MPData} />
					</App>
				</MemoryRouter>
	   	</Root>
	);
    expect(wrapped.find(Welcome)).toHaveLength(1);
    expect(wrapped.find(SearchBox)).toHaveLength(1);
    expect(wrapped.find(MPData)).toHaveLength(0);

    wrapped.unmount();
});


it('can see mp data upon entering valid postal code', () => {
	const wrapped = mount(
		 <Root>
			 <MemoryRouter initialEntries={[ '/' ]} >
				 <App>
					 <Route path="/" exact component={Welcome} />
					 <Route path="/:postalCode" exact component={MPData} />
				 </App>
			 </MemoryRouter>
		 </Root>
 );

//create mock response for fetchJsonp in this test
 global.fetchJsonp = jest.fn().mockImplementation(() => Promise.resolve({ok: true, Id: '123'}));



//insert value into serach bar and click search
wrapped.find('.searchbar').simulate('change', { target: { value: 'm2r 2s7' } });
wrapped.update();
expect(wrapped.find('.searchbar').prop('value')).toEqual('m2r 2s7');

//seems to be diffuclt to test react router history push
//i may leavce this for now
wrapped.find('.btn-submit').simulate('click');
wrapped.update();
expect(wrapped.find('.searchbar').prop('value')).toEqual('');


//check if resulting page displays appropriate mp info

wrapped.unmount();
});
//user eenters postal code and mpdata page with postal code is
//disalayed - will need to do api mocking



// //it laods mp data with valid postal code
// it('loads mp data with valid postal code', done => {
//   //atttempt to render app
// 	const wrapped = mount(
// 		<Root>
// 			<App />
// 		</Root>
// 	);
//
//   wrapped.find
//
//
//   //dont forget to call wrapped.unmount
// });
