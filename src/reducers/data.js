import { FETCH_DATA, SET_ERROR } from 'actions/types';

const INITIAL_STATE = {
	mpdata: '',
	error: ''
}

export default function(state = INITIAL_STATE, action) {
	switch(action.type) {
		case FETCH_DATA:
			return {
				...state,
				mpdata: action.payload
			};
		case SET_ERROR:
			return { ...state, error: action.payload };
		default:
			return state;
	}
}
