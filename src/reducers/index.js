import { combineReducers } from 'redux';

import dataReducer from 'reducers/data';

export default combineReducers({ data: dataReducer });
