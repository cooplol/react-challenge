import { FETCH_DATA, SET_ERROR } from 'actions/types';
import dataReducer from 'reducers/data';

//exmple data returned by api
const MPDataJSON = {"city": "North York", "province": "ON", "boundaries_centroid": [{"external_id": "35115", "boundary_set_name": "Federal electoral district", "url": "/boundaries/federal-electoral-districts/35115/", "name": "Willowdale", "related": {"boundary_set_url": "/boundary-sets/federal-electoral-districts/"}}], "representatives_centroid": [{"source_url": "http://www.parl.gc.ca/Parliamentarians/en/members?view=ListAll", "party_name": "Liberal", "personal_url": "http://aehsassi.liberal.ca", "first_name": "Ali", "offices": [{"tel": "1 613 992-4964", "fax": "1 613 992-1158", "postal": "House of Commons\nOttawa ON  K1A 0A6", "type": "legislature"}, {"tel": "1 416 223-2858", "fax": "1 416 223-9715", "postal": "115 Sheppard Avenue West (Main Office)\n\nToronto ON  M2N 1M7", "type": "constituency"}], "representative_set_name": "House of Commons", "photo_url": "http://www.ourcommons.ca/Parliamentarians/Images/OfficialMPPhotos/42/EhsassiAli_Lib.jpg", "related": {"boundary_url": "/boundaries/federal-electoral-districts/35115/", "representative_set_url": "/representative-sets/house-of-commons/"}, "email": "Ali.Ehsassi@parl.gc.ca", "extra": {}, "district_name": "Willowdale", "name": "Ali Ehsassi", "elected_office": "MP", "last_name": "Ehsassi", "url": "http://www.parl.gc.ca/Parliamentarians/en/members/Ali-Ehsassi(89010)", "gender": ""}], "boundaries_concordance": [], "centroid": {"type": "Point", "coordinates": [-79.444103, 43.792415]}, "code": "M2R2S8"};

it('handles actions of type FETCH_DATA', () => {
  const action = {
    type: FETCH_DATA,
    payload: MPDataJSON
  };

  const newState = dataReducer(undefined, action);

  expect(newState.mpdata).toEqual(MPDataJSON);
  expect(newState.error).toEqual('');
});

it('handles actions of type SET_ERROR', () => {
  const action = {
    type: SET_ERROR,
    payload: '404'
  };

  const newState = dataReducer(undefined, action);

  expect(newState.mpdata).toEqual('');
  expect(newState.error).toEqual('404');
});

it('handles actions of unknown type', () => {
  const newState = dataReducer(undefined, { type: 'sdfsd'});

  expect(newState.mpdata).toEqual('');
  expect(newState.error).toEqual('');
});
