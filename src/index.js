import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';


import Root from 'Root';
import App from 'components/App';
import MPData from 'components/MPData';
import Welcome from 'components/Welcome';
// import Error404 from 'components/Error404';

//will look into redirection such that routes are child of
//App, and app will show children (route component) in
//the appropriate place of its code - i may have to
//abandon the idea of a 404 page for now and perhaps
//try to find a way to integrate it later



//using tnis 'Root' technique becuase it helps faciliate
//cleaner JEST testing with code reuse for a react/redux
//application
ReactDOM.render(
	<Root>
		<BrowserRouter>
			<App>
				<Route path="/" exact component={Welcome} />
				<Route path="/:postalCode" exact component={MPData} />
			</App>
		</BrowserRouter>
	</Root>
,
	document.querySelector('#root')
);
